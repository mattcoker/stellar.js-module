<?php

function stellar_admin_settings($form, &$form_state) {

	$element_counter = variable_get('stellar_element_counter', 1);
	variable_set('stellar_remove_id', 0);



  $form['#tree'] = TRUE;

	$form['stellar_element_fieldset'] = array(
		'#type' 	=> 'fieldset',
		'#prefix' => '<div id="stellar-fieldset-wrapper">',
		'#suffix' => '</div>',
	);

	for ($i = 1; $i <= $element_counter; $i++) {
		//Parent Container
		$form['stellar_element_fieldset']['element-'. $i] = array(
			'#type' 				=> 'fieldset',
			'#collapsible' 	=> TRUE,
			'#collapsed' 		=> FALSE,
			'#title' 				=> t('Stellar Element ' . $i),
			'#prefix' 			=> "<div id='element-block-$i' class='element-block'>",
			'#suffix' 			=> '</div>',
		);

		$form['stellar_element_fieldset']['element-'. $i]['title'] = array(
	    '#title' 					=> t("Element Name (optional)"),
	    '#type' 					=> 'textfield',
	    '#default_value' 	=> t('Put anything you want here'), 
	    '#size' 					=> 40,
		);

		$form['stellar_element_fieldset']['element-'. $i]['selector'] = array(
	    '#title' 					=> t("Selector"),
	    '#type' 					=> 'textfield',
	    '#default_value' 	=> t(''), 
	    '#size' 					=> 40,
		);

		$form['stellar_element_fieldset']['element-' . $i]['parallax-types'] = array(
			'#type' 					=> 'radios',
			'#title' 					=> t('Parallax Type'),
			'#options' 				=> array(
				'page' 						=> 'Page Position',
				'background' 			=> 'Background Position'
			),
			'#default_value' 	=> 'page',
			'#description' 		=> t('This allows you to choose whether to animate the element page position or the background position'),
		);

		$form['stellar_element_fieldset']['element-'. $i]['position-slider'] = array(
	    '#title' 					=> t("Positioning Ratio"),
	    '#type' 					=> 'slider',
	    '#default_value' 	=> 1,
	    '#slider_style' 	=> 'purple',
	    '#range' 					=> 'min',
	    '#min' 						=> 0,
	    '#max' 						=> 25,
	    '#step' 					=> .01,
	    '#size'						=> 6,
		);

		$form['stellar_element_fieldset']['element-'. $i]['horiz-offset-slider'] = array(
	    '#title' 					=> t("Horizontal Offset"),
	    '#type' 					=> 'slider',
	    '#slider_style' 	=> 'green',
	    '#default_value'  => 0,
	    '#range' 					=> 'min',
	    '#min' 						=> -5000,
	    '#max' 						=> 5000,
	    '#step' 					=> .1,
	    '#size'						=> 6,
		);

		$form['stellar_element_fieldset']['element-'. $i]['vert-offset-slider'] = array(
	    '#title' 					=> t("Vertical Offset"),
	    '#type' 					=> 'slider',
	    '#slider_style' 	=> 'blue',
	    '#range' 					=> 'min',
	    '#default_value'  => 0,
	    '#min' 						=> -5000,
	    '#max' 						=> 5000,
	    '#step' 					=> .1,
	    '#size'						=> 6,
		);

		$form['stellar_element_fieldset']['element-'. $i]['offset-parent'] = array(
	    '#title' 					=> t("Offset Parent"),
	    '#description' 		=> t('This text is to offset the parent'),
	    '#type' 					=> 'checkbox',
	    '#default_value'  => 'unchecked',
		);

		if ($i > 1) {
			$form['stellar_element_fieldset']['element-'. $i]['remove'] = array(
				'#type' 	=> 'submit',
				'#value' 	=> t('Remove'),
				'#submit' => array('stellar_remove_one'),
				'#ajax' 	=> array(
					'callback' 	=> 'stellar_callback',
					'wrapper' 	=> 'stellar-fieldset-wrapper',
					'method' 		=> 'replace',
				),
			);
		};
	}

	$form['add'] = array(
		'#type' 	=> 'submit',
		'#value' 	=> t('Add Element'),
		'#submit' => array('stellar_add_one'),
		'#ajax' 	=> array(
			'callback' 	=> 'stellar_callback',
			'wrapper'		=> 'stellar-fieldset-wrapper',
			'method' 		=> 'replace',
		),
	);

	$form['submit'] = array(
		'#type' 	=> 'submit',
		'#value' 	=> t('Save'),
		'#submit' => array('stellar_form_submit'),
	);

	$form['reset'] = array(
      '#type' 			=> 'submit',
      '#attributes' => array('class' => array('my-reset-button-class')),
      '#value' 			=> t('Reset All (cannot be undone)'),
      '#submit' 		=> array('stellar_reset')
  );
	dsm($form);
	return $form;
}

function stellar_callback($form, &$form_state) {
	return $form['stellar_element_fieldset'];
}

function stellar_add_one($form, &$form_state) {
	$increment = variable_get('stellar_element_counter', 1);
	$increment++;
	variable_set('stellar_element_counter', $increment);
	$form_state['rebuild'] = TRUE;
}

function stellar_remove_one($form, &$form_state) {
	$decrement = variable_get('stellar_element_counter');
	$form_item = variable_get('stellar_remove_id');
	if ($decrement > 1) {
		$decrement--;
		variable_set('stellar_element_counter', $decrement);
		unset($form_item);
	}
	$form_state['rebuild'] = TRUE;
}

function stellar_reset($form, &$form_state) {
	variable_set('stellar_element_counter', NULL);
}

function stellar_form_submit($form, &$form_state) {

	$element_counter = variable_get('stellar_element_counter', 1);

	$values = array();

	for($i = 1; $i <= $element_counter; $i++) {
		$curr_element = array(
			'collapsed' 			=> $form['stellar_element_fieldset']["element-$i"]['#collapsed'],
			'name_opt' 				=> $form['stellar_element_fieldset']["element-$i"]['title']['#default_value'],
			'selector' 				=> $form['stellar_element_fieldset']["element-$i"]['selector']['#default_value'],
			'parallax-types' 	=> $form['stellar_element_fieldset']["element-$i"]['parallax-types']['#default_value'],
			'main_ratio' 			=> $form['stellar_element_fieldset']["element-$i"]['position-slider']['#default_value'],
			'bg_ratio' 				=> $form['stellar_element_fieldset']["element-$i"]['position-slider']['#default_value'],
			'horiz_offset' 		=> $form['stellar_element_fieldset']["element-$i"]['horiz-offset-slider']['#default_value'],
			'vert-offset' 		=> $form['stellar_element_fieldset']["element-$i"]['vert-offset-slider']['#default_value'],
			'offset_parrent' 	=> $form['stellar_element_fieldset']["element-$i"]['offset-parent']['#default_value'],
		);
		dsm($curr_element);
	}


	// db_insert('stellar_table')
	// 	->fields(array(
	// 		'collapsed' => TRUE,
	// 		'name_opt' => "hello",
	// 		'selector' => "page",
	// 		'main_ratio' => 44,
	// 		'bg_ratio' => 5.3,
	// 		'horiz_offset' => 20,
	// 		'vert_offset' => 35,
	// 		'offset_parent' => TRUE,
	// 	))
	// 	->execute();
}






